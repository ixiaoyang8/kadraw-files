File format:
* [Flow Chart](https://www.kadraw.com/diagram) (*.iodraw)
* [Mind Map](https://www.kadraw.com/mind) (*.mind)
* [Gantt Chart](https://www.kadraw.com/gantt) (*.gantt)
* [Whiteboard](https://www.kadraw.com/whiteboard) (*.wb)
* [Code Chart](https://www.kadraw.com/codechart) (*.md)
* [Online Chart](https://www.kadraw.com/chart) (*.chart)
